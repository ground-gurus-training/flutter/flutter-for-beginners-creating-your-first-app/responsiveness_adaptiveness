import 'package:flutter/material.dart';

class LayoutBuilderPage extends StatelessWidget {
  const LayoutBuilderPage({super.key});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxWidth <= 1024) {
        return _buildPortraitScreen();
      } else {
        return _buildLandscapeScreen();
      }
    });
  }

  Widget _buildPortraitScreen() {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                'Button 1',
                style: TextStyle(fontSize: 24.0),
              ),
            ),
            const SizedBox(
              height: 16.0,
            ),
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                'Button 2',
                style: TextStyle(fontSize: 24.0),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLandscapeScreen() {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                'Button 1',
                style: TextStyle(fontSize: 24.0),
              ),
            ),
            const SizedBox(
              width: 16.0,
            ),
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                'Button 2',
                style: TextStyle(fontSize: 24.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
