import 'package:flutter/material.dart';

import 'layout_builder.dart';
// import 'media_query.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Ground Gurus'),
        ),
        body: const LayoutBuilderPage(),
        // body: const MediaQueryPage(),
      ),
    );
  }
}
